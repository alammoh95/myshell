#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <mcheck.h>

#include "parser.h"
#include "shell.h"

/**
 * Program that simulates a simple shell.
 * The shell covers basic commands, including builtin commands 
 * (cd and exit only), standard I/O redirection and piping (|). 
 
 */

#define MAX_DIRNAME 100
#define MAX_COMMAND 1024
#define MAX_TOKEN 128

/* Functions to implement, see below after main */
int execute_cd(char** words);
int execute_nonbuiltin(simple_command *s);
int execute_simple_command(simple_command *cmd);
int execute_complex_command(command *cmd);


int main(int argc, char** argv) {
	
	char cwd[MAX_DIRNAME];           /* Current working directory */
	char command_line[MAX_COMMAND];  /* The command */
	char *tokens[MAX_TOKEN];         /* Command tokens (program name, 
					  * parameters, pipe, etc.) */

	while (1) {

		/* Display prompt */		
		getcwd(cwd, MAX_DIRNAME-1);
		printf("%s> ", cwd);
		
		/* Read the command line */
		fgets(command_line, MAX_COMMAND, stdin);
		/* Strip the new line character */
		if (command_line[strlen(command_line) - 1] == '\n') {
			command_line[strlen(command_line) - 1] = '\0';
		}
		
		/* Parse the command into tokens */
		parse_line(command_line, tokens);

		/* Check for empty command */
		if (!(*tokens)) {
			continue;
		}
		
		/* Construct chain of commands, if multiple commands */
		command *cmd = construct_command(tokens);
		//print_command(cmd, 0);
		int exitcode = 0;
		if (cmd->scmd) {
			exitcode = execute_simple_command(cmd->scmd);
			if (exitcode == -1) {
				break;
			}
		}
		else {
			exitcode = execute_complex_command(cmd);
			if (exitcode == -1) {
				break;
			}
		}
		release_command(cmd);
	}
    
	return 0;
}


/**
 * Changes directory to a path specified in the words argument;
 * For example: words[0] = "cd"
 *              words[1] = "csc209/assignment3/"
 * Your command should handle both relative paths to the current 
 * working directory, and absolute paths relative to root,
 * e.g., relative path:  cd csc209/assignment3/
 *       absolute path:  cd /u/bogdan/csc209/assignment3/
 */
int execute_cd(char** words) {
	
	//Ensure arguments and command is not empty
	 if (words == NULL){
		  perror("cd");
		  return(EXIT_FAILURE);
	 }
	 //Ensure command is given
	 else if(words[0]==NULL){
		 perror("cd");
		 return(EXIT_FAILURE);
	 }
	 //Ensure argument is given
	 else if(words[1]==NULL){
		 perror("cd");
		 return(EXIT_FAILURE);
	 }
	 //Ensure there aren't too many arguments
	 else if(words[2]){
		 perror("cd");
		 return(EXIT_FAILURE);
	 }
	 //Ensure cd argument is given
	 else if(!(strcmp(words[0], "cd")==0)){
		 perror("cd");
		 return(EXIT_FAILURE);
	 }

	 char cwd[MAX_DIRNAME];
	 //Determine if argument is relative or not
	 if(is_relative(words[1])==1){
		 //Is relative
		 getcwd(cwd, MAX_DIRNAME-1);
		 //Concatenate destination
		 strcat(cwd, "/");
		 strcat(cwd, words[1]);
		 int result = chdir(cwd);
		 if (result == -1){
			 perror("words[1]");
			 return(EXIT_FAILURE);
		 }
	 }
	 else{
		 //Full path given
		 int result = chdir(words[1]);
		 if (result == -1){
			 perror("words[1]");
			 return(EXIT_FAILURE);
		 }
	 }
	 
}


/**
 * Executes a program, based on the tokens provided as 
 * an argument.
 * For example, "ls -l" is represented in the tokens array by 
 * 2 strings "ls" and "-l", followed by a NULL token.
 * The command "ls -l | wc -l" will contain 5 tokens, 
 * followed by a NULL token. 
 */
int execute_command(char **tokens) {
	
	int result;
	
	//Execute the command
	if ((result = execvp(tokens[0], tokens)) == -1){
		//Execute error message
		perror(tokens[0]);
		return EXIT_FAILURE; 
	}
	
}


/**
 * Executes a non-builtin command.
 */
int execute_nonbuiltin(simple_command *s) {
	/**
	 * TODO: Check if the in, out, and err fields are set (not NULL),
	 * and, IN EACH CASE:
	 * - Open a new file descriptor (make sure you have the correct flags,
	 *   and permissions);
	 * - redirect stdin/stdout/stderr to the corresponding file.
	 *   (hint: see dup2 man pages).
	 * - close the newly opened file descriptor in the parent as well. 
	 *   (Avoid leaving the file descriptor open across an exec!) 
	 * - finally, execute the command using the tokens (see execute_command
	 *   function above).
	 * This function returns only if the execution of the program fails.
	 */
	 //Redirect input
	if (s->in){
		printf("%s\n", s->in);
		int fd = open(s->in, O_RDONLY);
		if (fd == -1) {
			perror("open");
			exit(1);
		}
		int ret = dup2(fd, 0);
		if(ret != 0){
            fprintf(stderr, "Redirect input fail\n");
            return EXIT_FAILURE;
        }
		close(fd);
	}
	
	//Redirect output
	if (s->out){
		int fd = open( s->out, O_WRONLY | O_CREAT, 0700);
		if (fd == -1) {
			perror("open");
			exit(1);
		}
        int ret = dup2(fd, 1);
        close(fd);
        if(ret != 1){
            fprintf(stderr, "Redirect output fail\n");
            return EXIT_FAILURE;
        }
	}
	
	//Redirect error
	if (s->err){
		//open file descriptor
		int fd = open( s->err, O_WRONLY | O_CREAT, 0700);
		if (fd == -1) {
			perror("open");
			exit(1);
		}
        int ret = dup2(fd, 2);
        close(fd);
        if(ret != 2){
            fprintf(stderr, "Redirect error fail\n");
            return EXIT_FAILURE;
        }
	}
	//execute_command
	execute_command(s->tokens);
	
}


/**
 * Executes a simple command (no pipes).
 */
int execute_simple_command(simple_command *cmd) {
	 //Determine if command is built in
	 if (cmd->builtin == 1){
		 //exit
		 if (strcmp(cmd->tokens[0],"exit")==0){
			 return -1;
		 }
		 //cd
		 else if (strcmp(cmd->tokens[0],"cd")==0){
			 execute_cd(cmd->tokens);
			 return 0;
		 }
	 }
	 //execute non-builtin commands
	 else{
		pid_t child;
		int status, exit_status;
		//fork process to execute command
		if ((child = fork()) == 0) {
			execute_nonbuiltin(cmd);
		}
		//error handling
		else if(child == -1) {
			perror("fork");
			exit(1);
		}
		else{
			//wait for child
			while (waitpid(child, &status, WNOHANG) == 0) {
				sleep(1);
			}
			if (WIFEXITED(status)) {
				exit_status = WEXITSTATUS(status);
			}
			return 0;
		}
	 }
	
}


/**
 * Executes a complex command.  A complex command is two commands chained 
 * together with a pipe operator.
 */
int execute_complex_command(command *c) {
	
	//Execute simple command
	if (c->scmd){
		execute_nonbuiltin(c->scmd);
	}
	//execute each command separately
	else{
		//pipe file descriptor
		int fd[2], child, child2;
		if (pipe(fd) == -1) {
			perror ("pipe"); exit(1);
		}
		//fork child for first command
		child = fork();
		if (child  == 0) {
			close(fd[0]);
			int ret = dup2(fd[1], STDOUT_FILENO); //redirect output to next command
			close(fd[0]);
			if(ret < 0){
				fprintf(stderr, "Redirect output fail\n");
				return EXIT_FAILURE;
			}
			//execute command recursively
			execute_complex_command(c->cmd1);
			exit(1);
		}
		//Does not fork successfully
		else if(child == -1) {
			perror("fork");
			exit(1);
		}
		else{
			//fork process for second command
			child2 = fork();
			if (child2 == 0){
				close(fd[1]);
				int ret = dup2(fd[0], STDIN_FILENO); //redirect input from prev command
				close(fd[0]);
				if(ret < 0){
					fprintf(stderr, "Redirect output fail\n");
					return EXIT_FAILURE;
				}
				//execute command recursively
				execute_complex_command(c->cmd2);
				exit(1);
		}
			else if(child2 == -1) {
				perror("fork");
				exit(1);
			}
			//close file descriptor in parent
			close(fd[0]);
			close(fd[1]);
			//Wait for child processes
			wait(NULL);
			wait(NULL);
		
		}
	}
	return 0;
}
