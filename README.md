# myShell
My own mini-shell that is able to launch commands in new processes within UNIX systems, written in C
Run this on UNIX systems for best results. Linux all the way <3
Run 'make' to be sure that the shell program compiles without warnings or error.